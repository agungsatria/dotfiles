Dream
When you′re feelin' blue
Dream
That′s the thing to do
Dream
While the smoke rings rise in the air
You'll find your share of memories there
So dream
When the day is through
Dream
And they might come true
Things never are as bad as they seem
So dream
Dream
Dream

Things never are as bad as they seem
So dream
Dream
Dream
