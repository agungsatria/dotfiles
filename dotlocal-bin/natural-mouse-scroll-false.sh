#!/bin/bash
#Purpose: me-nonaktigkan natural scroll, di i3
#Version: 
#Created Date:  Kam 13 Jan 2022 05:09:31 WIB
#Modified Date: 
#Author: Agung Satria
# START #

# Robot
xinput set-prop pointer:"Compx 2.4G Receiver Mouse" "libinput Natural Scrolling Enabled" 0

# # Logitech
# Logitech
# xinput set-prop pointer:"Logitech USB Optical Mouse" "libinput Natural Scrolling Enabled" 0


# END #
